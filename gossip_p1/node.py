'''
These seem like they should be rules

- Only the most recent message is stored as non-aware to the peer
- If a peer 'wants' a message ahead of my verison, I assume they are aware the previous messages and will thus 'want' it on their own
- If a peer 'wants' a message ahead of my version, I then need to want that one and all previous
- If a peer shares a rumor with me, I assume they are aware of that one and all previous
- I should only restore as the same endpoint if I remember all of my old messages. Maybe can get away with out knowing anything about peers
- My want list should represent the heightest continous from 0 that I have seen


'''

from collections import defaultdict
import random
import uuid

class MessageStore(object):

    def __init__(self):
        self.largest_continous_idx = -1
        self.messages = []

    def add_message(self, message):
        message_num = int(message['Rumor']['MessageID'].split(":")[1].strip())

        while len(self.messages) <= message_num:
            self.messages.append(None)
        self.messages[message_num] = message

        i = self.messages.index.index(None) if None in self.messages else -1
        self.largest_continous_idx = i - 1

        if self.largest_continous_idx == -2:
            self.largest_continous_idx = len(self.messages)-1

    def get_messages_after(self, message_num):
        messages = [m for m in self.messages[message_num+1:] if m is not None]
        return messages

def randomly(seq):
    shuffled = list(seq)
    random.shuffle(shuffled)
    return iter(shuffled)

class Node(object):
    def __init__(self):
        self.peers = {}
        self.messages = {}

    def store_rumor(self, message):
        message_id = message['Rumor']['MessageID'].split(":")[0].strip()
        message_num = int(message['Rumor']['MessageID'].split(":")[1].strip())
        ms = self.messages.get(message_id, MessageStore())
        self.messages[message_id] = ms

        ms.add_message(message)

        from_peer = message['EndPoint']
        if from_peer is None:
            return

        if from_peer not in self.peers:
            self.peers[from_peer] = {}
        self.peers[from_peer][message_id] = max(message_num, self.peers.get(message_id, 0))

    def store_wants(self, want):
        self.peers[want['EndPoint']] = want["Wants"]

    def generate_wants(self):
        tmp_wants = {}
        for message_id, message in self.messages.iteritems():
            message_num = message.largest_continous_idx
            if message_num == -1:
                continue
            tmp_wants[message_id]=message_num
        return tmp_wants

    def generate_rumor(self):
        for peer, wants in randomly(self.peers.iteritems()):
            my_wants = self.generate_wants()
            for k in my_wants.keys():
                my_wants[k] = -1
            their_wants = self.peers.get(peer, {})
            my_wants.update(their_wants)
            for want, num in randomly(my_wants.iteritems()):
                if want not in self.messages:
                    continue
                # for message in randomly(self.messages[want].get_messages_after(num)):
                #     return peer, message
                messages = self.messages[want].get_messages_after(num)
                if len(messages) > 0:
                    return peer, messages[0]
        return None, None

class NodeHandler(object):
    def __init__(self, name):
        self.node = Node()
        self.default_message_id = str(uuid.uuid4())
        self.default_message_num = 0
        self.name = name

    def add_peer(self, peer_address):
        if peer_address not in self.node.peers:
            self.node.peers[peer_address] = {}

    def validate(self, message):
        return True

    def create_rumor(self, message_str):
        msg = {
            "Rumor": {
                "MessageID": self.default_message_id+":"+str(self.default_message_num),
                "Originator": self.name,
                "Text": message_str
            },
            "EndPoint": None
        }
        self.node.store_rumor(msg)
        self.default_message_num += 1

    def receive(self, message):
        if not self.validate(message):
            raise("Bad request")
        if "Rumor" in message:
            self.node.store_rumor(message)
        elif "Wants" in message:
            self.node.store_wants(message)
        else:
            raise Exception("Error!!! Bad message type")

    def send_loop(self):

        send_rumor = random.choice([True, False])
        if send_rumor:
            peer, msg = self.node.generate_rumor()
            if peer is None:
                send_rumor = False

        if not send_rumor:
            wants = self.node.generate_wants()
            msg = {
                "Wants": wants,
                "EndPoint": None
            }
            peers = self.node.peers.keys()
            peer = None
            if len(peers) > 0:
                peer = random.choice(peers)

        return peer, msg
