from flask import Flask, redirect, request, jsonify, render_template
app = Flask(__name__)
import node
import uuid
import random
import atexit
import logging
logging.basicConfig()

random.seed(0)

BASED_ADDRESS = "http://localhost:8000"

nodes = {}
for i in range(5):
    node_id = str(uuid.UUID(int=random.getrandbits(128)))
    nodes[node_id] = node.NodeHandler("Client {}".format(i))

# for n_id, n in nodes.iteritems():
#
n_keys = list(nodes.keys())
for i in xrange(len(n_keys)):
    n = nodes[n_keys[i]]
    next_idx = (i+1) % len(n_keys)
    node_url = BASED_ADDRESS+"/api/nodes/{}/gossip".format(n_keys[next_idx])
    n.add_peer(node_url)

@app.route('/nodes', methods=['GET'])
def list_nodes():
    output = "<a href='nodes/create'>Create new node</a><br><br>"
    sorted_keys = sorted(nodes.keys(), key=lambda x: nodes[x].name)
    for node_id in sorted_keys:
        node = nodes[node_id]
        output += """
        <a href='nodes/{}'>{} ({})</a><br>
        """.format(node_id, node.name, node_id)
    return output

@app.route('/nodes/create', methods=['GET'])
def create_node():
    return app.send_static_file('create_node.html')

@app.route('/nodes/<node>', methods=['GET'])
def display_node(node):
    return app.send_static_file('node.html')

@app.route('/api/nodes', methods=['POST'])
def api_node_post():
    m = request.get_json(force=True)
    originator = m['originator']
    node_id = str(uuid.UUID(int=random.getrandbits(128)))
    nodes[node_id] = node.NodeHandler(originator)
    return jsonify(**{'node_id': node_id})

@app.route('/api/nodes/<node>', methods=['GET'])
def api_node(node):
    n = nodes[node]
    messages = n.node.messages
    message_ls = []
    for m_id, ms in messages.iteritems():
        message_ls.append(ms.messages[:ms.largest_continous_idx+1])

    peers_ls = []
    for p in n.node.peers.keys():
        peers_ls.append(p)
    peers_ls.sort()

    return jsonify(**{'messages': message_ls, 'peers': peers_ls})

@app.route('/api/nodes/<node>/peers', methods=['POST'])
def api_add_peer(node):
    m = request.get_json(force=True)
    n = nodes[node]
    n.add_peer(m['peer'])
    return jsonify(**{'success': True})

@app.route('/api/nodes/<node>/messages', methods=['POST'])
def api_node_message(node):
    n = nodes[node]
    message = request.get_json(force=True)['message']
    n.create_rumor(message)
    return jsonify(**{'success': True})

@app.route('/api/nodes/<node>/gossip', methods=['POST'])
def api_endpoint(node):
    m = request.get_json(force=True)
    n = nodes[node]
    n.receive(m)
    return jsonify(**{'success': True})

@app.route('/reset/fiveloop', methods=['GET'])
def reset_fiveloop():
    global nodes
    nodes = {}
    for i in range(5):
        node_id = str(uuid.uuid4())
        nodes[node_id] = node.NodeHandler("Client {}".format(i))

    n_keys = list(nodes.keys())
    for i in xrange(len(n_keys)):
        n = nodes[n_keys[i]]
        next_idx = (i+1) % len(n_keys)
        node_url = BASED_ADDRESS+"/api/nodes/{}/gossip".format(n_keys[next_idx])
        n.add_peer(node_url)

    return redirect(BASED_ADDRESS+'/nodes')

@app.route('/reset/twofourloops', methods=['GET'])
def reset_twofourloop():
    global nodes
    nodes = {}
    group_1_keys = []
    for i in range(4):
        node_id = str(uuid.uuid4())
        node_group_1 = node_id
        nodes[node_id] = node.NodeHandler("Client {}".format(i))
        group_1_keys.append(node_id)

    for i in xrange(len(group_1_keys)):
        n = nodes[group_1_keys[i]]
        next_idx = (i+1) % len(group_1_keys)
        node_url = BASED_ADDRESS+"/api/nodes/{}/gossip".format(group_1_keys[next_idx])
        n.add_peer(node_url)

    group_2_keys = []
    for i in range(4, 8):
        node_id = str(uuid.uuid4())
        node_group_2 = node_id
        nodes[node_id] = node.NodeHandler("Client {}".format(i))
        group_2_keys.append(node_id)

    for i in xrange(len(group_2_keys)):
        n = nodes[group_2_keys[i]]
        next_idx = (i+1) % len(group_2_keys)
        node_url = BASED_ADDRESS+"/api/nodes/{}/gossip".format(group_2_keys[next_idx])
        n.add_peer(node_url)

    group_1 = nodes[node_group_1]
    group_1.add_peer(BASED_ADDRESS+"/api/nodes/{}/gossip".format(node_group_2))
    return redirect(BASED_ADDRESS+'/nodes')

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger
# from requests import async
import requests

def run_all_loop():
    for node_id, node in nodes.iteritems():
        peer, msg = node.send_loop()
        if peer is None:
            continue
        my_endpoint = "http://localhost:8000/api/nodes/{}/gossip".format(node_id)
        msg['EndPoint'] = my_endpoint
        r = requests.post(peer, json=msg)

scheduler = BackgroundScheduler()
scheduler.start()
scheduler.add_job(
    func=run_all_loop,
    trigger=IntervalTrigger(seconds=1),
    replace_existing=True)
atexit.register(lambda: scheduler.shutdown())

if __name__ == '__main__':
    app.run(host='localhost', port=8000, threaded=True)
