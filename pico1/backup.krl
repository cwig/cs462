ruleset track_trips_mod {
    meta {
        name "Track Trips Mod"
        description << A first ruleset for the Lessons >>
        author "Echo"
        logging on
        shares e, __testing, hello
    }
    global {
        __testing = {
            "queries": [
                { "name": "__testing" }
            ],
            "events": [
                { "domain": "car", "type": "new_trip"},
                { "domain": "car", "type": "new_trip", "attrs":["mileage"]}
            ]
        }
        hello = function(obj) {
            msg = "Hello " + obj;
            msg
        }
    }
    rule process_trip {
        select when car new_trip
        send_directive("trip") with
            //trip_length = event:attr("mileage")
            trip_length = hello
        fired {
            raise explicit event "trip_processed"
                attributes event:attr
        }
    }
    rule find_long_trips {
        select when explicit trip_processed
        send_directive("trip") with
            trip_length = hello
    }
}
