ruleset echo {
  meta {
    name "Echo"
    description << A first ruleset for the Lessons >>
    author "Echo"
    logging on
    shares e, __testing
  }

  global {
      __testing = {
        "queries": [
            { "name": "__testing" }
        ],
        "events": [
            { "domain": "echo", "type": "hello" },
            { "domain": "echo", "type": "message", "attrs": ["message"] }
        ]
    }
  }

  rule hello {
    select when echo hello

    send_directive("say") with
      something = "Hello World"
  }

  rule message {
    select when echo message
    send_directive("say") with
        something = event:attr("message")
  }
}
