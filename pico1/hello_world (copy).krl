ruleset hello_world {
  meta {
    name "Hello World"
    description <<
A first ruleset for the Quickstart
>>
    author "Phil Windley"
    logging on
    shares hello
  }
  
  global {
    hello = function(obj) {
      msg = "Hello " + obj;
      msg
    }
  }
  
  rule hello_world {
  select when echo hello
  pre{
    name = event:attr("name").defaultsTo(ent:name,"use stored name")
  }
  send_directive("say") with
    something = "Hello " + name
  }
  
}
