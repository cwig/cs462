ruleset track_trips {
    meta {
        name "Track Trips"
        description << A first ruleset for the Lessons >>
        author "Echo"
        logging on
        shares e, __testing
    }
    global {
        __testing = {
            "queries": [
                { "name": "__testing" }
            ],
            "events": [
                { "domain": "echo", "type": "message", "attrs":["mileage"]}
            ]
        }
    }
    rule process_trip {
        select when echo message
        send_directive("trip") with
            trip_length = event:attr("mileage")
    }
}
