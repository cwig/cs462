ruleset manage_fleet {
  meta {
    name "Manage Fleet"
    logging on
    shares __testing, vehicles, trips, getScatterReports
    use module Subscriptions
    use module io.picolabs.pico alias wrangler
  }
  global {
    __testing = {
      "queries": [
        { "name": "vehicles" },
        { "name": "trips" },
        { "name": "getScatterReports" }
      ],
      "events": [
        {"domain": "car", "type": "new_vehicle", "attrs":["name"]},
        {"domain": "car", "type": "unneeded_vehicle", "attrs":["name"]},
        {"domain": "car", "type": "request_report"}
      ]
    }
    vehicles = function() {
      Subscriptions:getSubscriptions()
    }
    cloud = function(eci, func, params) {
        response = http:get("http://localhost:8080/sky/cloud/" + eci + "/trip_store/" + func, (params || {}).put(["_eci"], eci));
        response{"content"}.decode()
    }
    trips = function() {
      Subscriptions:getSubscriptions().filter(function(subcription){
        subcription{["attributes","subscriber_role"]} == "vehicle"
      }).map(function(subcription) {
        cloud(subcription{["attributes","subscriber_eci"]}, "trips")
      })
    }
    getScatterReports = function() {
      ent:reports_list.slice((ent:reports_list.length() < 5) => 0 | ent:reports_list.length()-5, ent:reports_list.length()-1).map(function(key) {
         tmp = ent:reports{key};
         tmp{"responding"} = tmp{"trips"}.length();
         tmp

      })
    }
  }
  rule create_vehicle {
     select when car new_vehicle
     always {
       raise pico event "new_child_request"
         attributes { "dname": event:attr("name"), "color": "#54ff65", "name": event:attr("name")}
     }
   }
   rule pico_child_initialized {
     select when pico child_initialized
     pre {
       the_vehicle = event:attr("new_child")
       vehicle_name = event:attr("rs_attrs"){"name"}
     }
     event:send({
       "eci": the_vehicle.eci, "eid": "install-ruleset",
       "domain": "pico", "type": "new_ruleset",
       "attrs": { "rid": "track_trips_mod" }
     })
     event:send({
       "eci": the_vehicle.eci, "eid": "install-ruleset",
       "domain": "pico", "type": "new_ruleset",
       "attrs": { "rid": "trip_store" }
     })
     fired {
       ent:vehicles := ent:vehicles || {};
       ent:vehicles{[vehicle_name]} := the_vehicle;
       raise wrangler event "subscription" with
         name = vehicle_name
         name_space = "car"
         my_role = "fleet"
         subscriber_role = "vehicle"
         channel_type = "subscription"
         subscriber_eci = the_vehicle.eci
     }
  }
  // delete_vehicle that responds to a car:unneeded_vehicle
  rule delete_vehicle {
    select when car unneeded_vehicle
    fired {
      raise pico event "delete_child_request"
        attributes ent:vehicles{event:attr("name")};
      ent:vehicles{[event:attr("name")]} := null;
      raise wrangler event "subscription_cancellation"
        with subscription_name = "car:" + event:attr("name")
    }
  }
  rule scatter_report_init {
    select when car request_report
    pre {
      uid = time:now().replace(".", "_")
      new_report = ent:reports || {}
      new_report{uid} = {
        "vehicles": Subscriptions:getSubscriptions().filter(function(subcription){
          subcription{["attributes","subscriber_role"]} == "vehicle"
        }).length(),
        "trips": []
      }

      new_report_list = ent:reports_list || []
      new_report_list = new_report_list.append(uid)
    }
    always {
      ent:reports := new_report;
      ent:reports_list := new_report_list;
      raise car event "request_report_with_uid"
          attributes {
            "uid": uid
          }
    }
  }
  rule scatter_report {
    select when car request_report_with_uid
    foreach Subscriptions:getSubscriptions() setting (subscription)
      pre {
        // x=event:attrs().klog("--------------first here--------------")
        // x=event:attrs().klog(wrangler:myself(){"eci"})
        subs_attrs = subscription{"attributes"}
      }
      if subs_attrs{"subscriber_role"} == "vehicle" then
        event:send(
          {
            "eci": subs_attrs{"subscriber_eci"},
            "eid": "report-needed",
            "domain": "car",
            "type": "report_needed",
            "attrs": {
              "eci": wrangler:myself(){"eci"},
              "uid": event:attr("uid")
            }
          }
      )
  }
  rule gather_report {
    select when car report

    pre {
      // x=event:attrs().klog("**************another here**********")
      uid = event:attr("uid")
      new_report = ent:reports
      tmp_array = new_report{[uid, "trips"]}
      tmp_dict = {}
      tmp_dict{"vehicle_trip"} = event:attr("trips")
      tmp_array = tmp_array.append(tmp_dict)
      new_report{[uid, "trips"]} = tmp_array
    }
    always {
      ent:reports := new_report
    }
  }
}
