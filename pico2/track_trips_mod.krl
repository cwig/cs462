ruleset track_trips_mod {
    meta {
        name "Track Trips Mod"
        description << A first ruleset for the Lessons >>
        author "Echo"
        logging on
        shares e, __testing, hello
        use module trip_store
    }
    global {
        __testing = {
            "queries": [
                { "name": "__testing" }
            ],
            "events": [
                { "domain": "car", "type": "new_trip"},
                { "domain": "car", "type": "new_trip", "attrs":["mileage"]}
            ]
        }
        long_trip = 100
    }
    rule process_trip {
        select when car new_trip
        send_directive("trip") with
            trip_length = event:attr("mileage")
        fired {
            raise explicit event "trip_processed"
                attributes {"mileage": event:attr("mileage"), "timestamp": time:now()}
        }
    }
    rule find_long_trips {
        select when explicit trip_processed
        if(event:attr("mileage").as("Number") > long_trip) then
            noop()
            fired {
                raise explicit event "found_long_trip"
                    attributes event:attrs()
            }
    }
    rule found_long_trip {
        select when explicit found_long_trip
        send_directive("trip") with
            long_trip = 1
    }
    rule auto_accept {
      select when wrangler inbound_pending_subscription_added
      pre {
        attributes = event:attrs().klog("subcription:")
      }
      always {
        raise wrangler event "pending_subscription_approval"
          attributes attributes
      }
    }
    rule report_needed {
      select when car report_needed
      pre {
        // x=event:attrs().klog("___________HERE___________")
      }
      event:send({
         "eci": event:attr("eci"), "eid": "report",
         "domain": "car", "type": "report",
         "attrs": { "trips": trip_store:trips(), "uid": event:attr("uid") }
      })
    }
}
