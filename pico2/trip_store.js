ruleset trip_store {
    meta {
        name "Trip Store"
        description << A first ruleset for the Lessons >>
        author "Echo"
        logging on
        shares e, __testing, trips, long_trips, short_trips
        provides trips, long_trips, short_trips
    }
    global {
        __testing = {
            "queries": [
                { "name": "trips" },
                { "name": "long_trips" },
                { "name": "short_trips" },
                { "name": "__testing" }
            ],
            "events": [
                { "domain": "car", "type": "trip_reset"}
            ]
        }
        long_trip = 100
        trips = function() {
            ent:trips
        }
        long_trips = function() {
            ent:long_trips
        }
        short_trips = function() {
            ent:trips.filter(function(x){
                x{"mileage"} <= long_trip
            })
        }
    }
    rule collect_trips {
        select when explicit trip_processed
        pre {
            mileage = event:attr("mileage").as("Number")
            timestamp = event:attr("timestamp")
        }
        always{
            ent:trips := ent:trips.append({"timestamp":timestamp, "mileage":mileage})
        }
    }
    rule collect_long_trips {
        select when explicit found_long_trip
        pre {
            mileage = event:attr("mileage").as("Number")
            timestamp = event:attr("timestamp")
        }
        always{
            ent:long_trips := ent:long_trips.append({"timestamp":timestamp, "mileage":mileage})
        }
    }
    rule clear_trips {
        select when car trip_reset
        always {
            ent:long_trips := [];
            ent:trips := []
        }
    }
}
