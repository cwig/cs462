from flask import Flask, redirect, request, jsonify
app = Flask(__name__)


mapping = {
    'foo': 'http://google.com'
}

@app.route('/print', methods=['GET'])
def redirect_route():
    param = next(iter(request.args.keys()))
    redirect_to = mapping.get(param, '/')
    return redirect(redirect_to, code=302)

@app.route('/redirect', methods=['GET'])
def redirect_route():
    param = next(iter(request.args.keys()))
    redirect_to = mapping.get(param, '/')
    return redirect(redirect_to, code=302)

@app.route('/version')
def version_route():
    mimetypes = ["text/html", "application/vnd.byu.cs462.v1+json", "application/vnd.byu.cs462.v2+json"]
    best = request.accept_mimetypes.best_match(mimetypes)
    if best == mimetypes[1]:
        return jsonify(**{"version": "v1" })
    elif best == mimetypes[2]:
        return jsonify(**{"version": "v2" })
    else:
        return jsonify(**{"version": "unknown" })

if __name__ == '__main__':
    app.run(host='localhost', port=8000, debug=True, threaded=True)
